<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

    <style media="screen">
      h1 {
        font-size: 18px;
        font-weight: bold;
      }
      .mb-50 {
        margin-top: 30px;
        margin-bottom: 20px;
      }
    </style>
</head>
<body>
  @if(session()->get('success'))
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        {{session('success')}}
    </div>
@endif

@if(session()->get('error'))
    <div class="alert alert-error alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        {{session('error')}}
    </div>
@endif

    <a href="{{ route('logout') }}" class="btn btn-info float-right" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">
        Logout
    </a>
    <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>
    <form method="post" action="{{ route('customer.store') }}">
    @csrf
        <div class="container">
          <div class="row mb-50">
            <div class="col-md-12">
              <h1>Customer Detail</h1>
            </div>
          </div>
          <div class="row">
            <div class="col-md-8">
                <h1>Name</h1>
                <div class="form-group">
                  <input type="text" name="name" placeholder="Name" class="form-control">
                </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-8">
                <h1>Email</h1>
                <div class="form-group">
                  <input type="text" name="email" placeholder="Email" class="form-control">
                </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-8">
                <h1>Phone</h1>
                <div class="form-group">
                  <input type="text" name="phone" placeholder="Phone" class="form-control">
                </div>
                <hr>
            </div>
          </div>
          <div class="row mb-50">
            <div class="col-md-12">
              <h1>Car Detail</h1>
            </div>
          </div>
          <div class="row">
            <div class="col-md-8">
                <h1>Car No:</h1>
                <div class="form-group">
                  <input type="text" name="car_name" placeholder="Car No:" class="form-control">
                </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-8">
                <h1>Registration No:</h1>
                <div class="form-group">
                  <input type="text" name="reg_no" placeholder="Reg No:" class="form-control">
                </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-8">
                <h1>Model No:</h1>
                <div class="form-group">
                  <input type="text" name="model" placeholder="Model No:" class="form-control">
                </div>
                <hr>
            </div>
          </div>
            <div class="row">
              <div class="col-md-12">
                  <h1>Select Date:</h1>
              </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                     <input class="date form-control" type="text" placeholder="Start Date" name="from_date">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <input class="date form-control" type="text" placeholder="End Date" name="to_date">
                  </div>
                </div>
            </div>
            <div class="row">
              <div class="col-md-8">
                  <select class="js-example-basic-multiple form-control" name="services[]" multiple="multiple">
        						@foreach($services as $service)
        							<option value="{{$service->id}}">{{$service->name}}</option>
        						@endforeach
        					</select>
              </div>
            </div>
            <div class="row">
              <div class="col-md-8">
                  <h1>Note:</h1>
                  <div class="form-group">
                    <textarea class="form-control" name="note" rows="3"></textarea>
                  </div>
              </div>
            </div>
            <button type="submit" class="btn btn-info">Book Now</button>
        </div>
    </form>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
    <script src="{{ asset('js/select2.min.js') }}"></script>
    <script type="text/javascript">
    $(document).ready(function(){
        $('.js-example-basic-multiple').select2();
    });
      $('.date').datepicker({
         format: 'yy/mm/dd',
         todayHighlight: true,
         autoclose: true,
       });
    </script>
</body>
</html>
