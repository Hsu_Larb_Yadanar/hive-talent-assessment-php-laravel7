@extends("dashboard.master")

@section ("header")

    <strong>Add Booking</strong>

@endsection

@section ('breadcrumb-li')

    <li class="active">Add Booking</li>

@endsection

@section("content")
<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        @if ($errors->any())
          <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
            </ul>
          </div>
        @endif
        <form method="post" action="{{ route('booking.store') }}">
          @csrf
          <h3>Customer Detail</h3>
          <div class="form-group">
              <label>Name:</label>
              <input type="text" name="name" placeholder="Name" class="form-control">
          </div>
          <div class="form-group">
              <label>Email:</label>
              <input type="email" name="email" placeholder="Email" class="form-control">
          </div>
          <div class="form-group">
              <label>Phone:</label>
              <input type="text" name="phone" placeholder="Phone" class="form-control">
          </div>
          <hr>
          <h3>Car Detail</h3>
          <div class="form-group">
              <label>Car Number:</label>
              <input type="text" name="car_name" placeholder="Car No:" class="form-control">
          </div>
          <div class="form-group">
              <label>Car Reg No:</label>
              <input type="text" name="reg_no" placeholder="Reg No:" class="form-control">
          </div>
          <div class="form-group">
              <label>Model No:</label>
              <input type="text" name="model" placeholder="Model No:" class="form-control">
          </div>
          <hr>
          <div class="form-group">
              <label>Select Date:</label>
              <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                       <input class="date form-control" type="text" placeholder="Start Date" name="from_date">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <input class="date form-control" type="text" placeholder="End Date" name="to_date">
                    </div>
                  </div>
              </div>
          </div>
          <div class="form-group">
              <label>Addtional services:</label>
              <select class="select2-multi-form form-control" name="services[]" multiple="multiple">
                @foreach($services as $service)
                  <option value="{{$service->id}}">{{$service->name}}</option>
                @endforeach
              </select>
          </div>
          <div class="form-group">
              <label>Note:</label>
              <textarea class="form-control" name="note" rows="3"></textarea>
          </div>
          <a href="{{ route('booking.index') }}" class="btn btn-info">Back</a>
          <button type="submit" class="btn btn-success">Submit</button>
        </form>
  		</div>
</div>
@endsection
