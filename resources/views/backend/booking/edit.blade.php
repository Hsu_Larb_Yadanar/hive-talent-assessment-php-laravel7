@extends("dashboard.master")

@section ("header")

    <strong>Edit Booking</strong>

@endsection

@section ('breadcrumb-li')

    <li class="active">Edit Booking</li>

@endsection

@section("content")
<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        @if ($errors->any())
          <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
            </ul>
          </div>
        @endif
        <form method="post" action="{{ route('booking.update', $booking->id ) }}">
          @csrf
          @method('PATCH')
            <div class="form-group">
                <label>Booking Id:</label>
                <input type="text" class="form-control" name="name" value="{{ $booking->customer->barcode_no }}" disabled/>
            </div>
            <div class="form-group">
                <label>Status</label>
                <select class="form-control" name="status">
                  <option value="option" disabled selected>Select Status</option>
                  <option value="0" {{$booking->status== 0  ? 'selected' : ''}}>Pending</option>
                  <option value="1" {{$booking->status== 1  ? 'selected' : ''}}>Paid</option>
                </select>
              </div>
            <div class="form-group">
                <label>Customer Name:</label>
                <input type="text" class="form-control" name="name" value="{{ $booking->customer->name }}"/>
            </div>
            <div class="form-group">
                <label>email:</label>
                <input type="email" class="form-control" name="email" value="{{ $booking->customer->email }}"/>
            </div>
            <div class="form-group">
                <label>Phone:</label>
                <input type="text" class="form-control" name="phone" value="{{ $booking->customer->phone }}"/>
            </div>
            <div class="form-group">
                <label>From Date:</label>
                <input class="date form-control" type="text" placeholder="Start Date" name="from_date" value="{{ $booking->customer->from_date }}">
            </div>
            <div class="form-group">
                <label>To Date:</label>
                <input class="date form-control" type="text" placeholder="End Date" name="to_date" value="{{ $booking->customer->to_date }}">
            </div>
            <div class="form-group">
                <label>Note:</label>
                <textarea class="form-control" name="note" rows="3">{{$booking->customer->note}}</textarea>
            </div>
            <div class="form-group">
                <label>Addtional services:</label>
                <select class="select2-multi-form form-control" name="services[]" multiple="multiple">
                  @foreach($services as $service)
                    <option value="{{$service->id}}">{{$service->name}}</option>
                  @endforeach
                </select>
            </div>
            <div class="form-group">
              <a href="{{ route('booking.index') }}" class="btn btn-info">Back</a>
              <button type="submit" class="btn btn-primary">Update</button>
            </div>
      </form>
  		</div>
</div>
@endsection
