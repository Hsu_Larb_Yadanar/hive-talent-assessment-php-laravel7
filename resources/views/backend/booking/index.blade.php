@extends("dashboard.master")

@section ("header")

    <strong>Bookings</strong>
    <a href="{{ route('booking.create') }}" class="btn btn-info">Add</a>

@endsection

@section ('breadcrumb-li')

    <li class="active">Bookings</li>

@endsection

@section("content")
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
  <div class="box box-primary">
          <div class="box-body">
              <div class="dataTables_wrapper form-inline dt-bootstrap">
                  <div class="table-responsive">
                    @if(session()->get('success'))
                      <div class="alert alert-success alert-dismissable">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                          </button>
                          {{session('success')}}
                      </div>
                    @endif

                    @if(session()->get('error'))
                        <div class="alert alert-error alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{session('error')}}
                        </div>
                    @endif
                    <table class="table table-bordered table-hover dataTable">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Booking ID</th>
                          <th>Name</th>
                          <th>Date From</th>
                          <th>Date To</th>
                          <th>Duration Date</th>
                          <th>Services</th>
                          <th>Status</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $i=1; ?>
                        @foreach( $bookings as $result )
                        <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $result->customer->barcode_no }}</td>
                        <td>{{ $result->customer->name }}</td>
                        <td>{{ $result->customer->from_date }}</td>
                        <td>{{ $result->customer->to_date }}</td>
                        <?php
                            $date1 = date_create($result->customer->from_date);
                            $date2 = date_create($result->customer->to_date);
                            $diff = date_diff($date1,$date2);
                        ?>
                        <td>{{ $diff->format("%a days") }}</td>
                        <td>
                            @foreach ($result->customer->service as $value)
                                {{$value->name}}
                            @endforeach
                        </td>
                        <td>
                          @if ( $result["status"] == 0 )
                            <span class="badge badge-warning">Pending</span>
                          @elseif ( $result["status"] == 1 )
                            <span class="badge badge-success">Paid</span>
                          @else
                            <span class="badge badge-danger">Reject</span>
                          @endif
                        </td>
                        <td>
                            <ul class="list-inline">
                              <li class="list-inline-item">
                                <a href="{{ route('booking.edit', $result->id) }}" class="btn btn-warning"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                              </li>
                              <li class="list-inline-item">
                                <form action="{{ route('booking.destroy', $result->id) }}" class="delete_form" method="post">
                                  @csrf
                                  @method('DELETE')
                                  <button type="submit" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                </form>
                              </li>
                            </ul>
                        </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
              </div>
          </div>
      </div>
</div>
</div>
@endsection
