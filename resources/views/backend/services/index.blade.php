@extends("dashboard.master")

@section ("header")

    <strong>Services</strong>
    <a href="{{ route('service.create') }}" class="btn btn-info">Add</a>

@endsection

@section ('breadcrumb-li')

    <li class="active">Services</li>

@endsection

@section("content")
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
  <div class="box box-primary">
          <div class="box-body">
              <div class="dataTables_wrapper form-inline dt-bootstrap">
                  <div class="table-responsive">
                    @if(session()->get('success'))
                      <div class="alert alert-success alert-dismissable">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                          </button>
                          {{session('success')}}
                      </div>
                    @endif

                    @if(session()->get('error'))
                        <div class="alert alert-error alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{session('error')}}
                        </div>
                    @endif
                    <table class="table table-bordered table-hover dataTable">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Name</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $i=1; ?>
                        @foreach($services as $result)
                        <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $result["name"] }}</td>
                        <td>
                            <ul class="list-inline">
                              <li class="list-inline-item">
                                <a href="{{ route('service.edit', $result->id) }}" class="btn btn-warning"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                              </li>
                              <li class="list-inline-item">
                                <form action="{{ route('service.destroy', $result->id) }}" class="delete_form" method="post">
                                  @csrf
                                  @method('DELETE')
                                  <button type="submit" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                </form>
                              </li>
                            </ul>
                        </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
              </div>
          </div>
      </div>
</div>
</div>
@endsection
