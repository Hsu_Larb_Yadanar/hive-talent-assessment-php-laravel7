@extends("dashboard.master")

@section ("header")

    <strong>Edit Services</strong>

@endsection

@section ('breadcrumb-li')

    <li class="active">Edit Service</li>

@endsection

@section("content")
<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        @if ($errors->any())
          <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
            </ul>
          </div>
        @endif
        <form method="post" action="{{ route('service.update', $service->id ) }}">
          @csrf
          @method('PATCH')
          <div class="form-group">
              <label>Name:</label>
              <input type="text" class="form-control" name="name" value="{{ $service->name }}"/>
          </div>
          <a href="{{ route('service.index') }}" class="btn btn-info">Back</a>
          <button type="submit" class="btn btn-primary">Update</button>
      </form>
  		</div>
</div>
@endsection
