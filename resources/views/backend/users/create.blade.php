@extends("dashboard.master")

@section ("header")

    <strong>Add user</strong>

@endsection

@section ('breadcrumb-li')

    <li class="active">Add user</li>

@endsection

@section("content")
<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        @if ($errors->any())
          <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
            </ul>
          </div>
        @endif
        <form method="post" action="{{ route('user.store') }}">
          @csrf
          <div class="form-group">
              <label>Name:</label>
              <input type="text" class="form-control" name="name"/>
          </div>
          <div class="form-group">
              <label>Email:</label>
              <input type="text" class="form-control" name="email" />
          </div>
          <div class="form-group">
              <label>Password:</label>
              <input type="password" class="form-control" name="password"/>
          </div>
          <div class="form-group">
              <label>Phone:</label>
              <input type="text" class="form-control" name="phone"/>
          </div>
          <div class="form-group">
              <label>Role:</label>
              <select class="form-control" name="is_admin">
                <option value="option" disabled selected>Select Role</option>
                <option value="1">Admin</option>
                <option value="2">User</option>
              </select>
          </div>
          <a href="{{ route('user.index') }}" class="btn btn-info">Back</a>
          <button type="submit" class="btn btn-success">Submit</button>
      </form>
  		</div>
</div>
@endsection
