<script src="{{ asset('js/login_main.js') }}"></script>
<script src="{{ asset('js/select2.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
<script type="text/javascript">
    function logout() {
        event.preventDefault();
        document.getElementById('logout-form').submit();
    }

    $(document).ready(function(){
        $('.select2-multi-form').select2();
    });

    $('.date').datepicker({
       format: 'yy/mm/dd',
       todayHighlight: true,
       autoclose: true,
     });
</script>
