<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('/upload/avatar2.png') }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ auth()->user()->name }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">Main Navigation</li>
            <li class="#">
                <a href="{{ route('booking.index') }}">
                    <i class="fa fa-file" aria-hidden="true"></i>
                    <span>Booking</span>
                </a>
            </li>
            <li class="#">
                <a href="{{ route('service.index') }}">
                    <i class="fa fa-file" aria-hidden="true"></i>
                    <span>Service</span>
                </a>
            </li>
            <li class="#">
                <a href="{{ route('user.index') }}">
                    <i class="fa fa-file" aria-hidden="true"></i>
                    <span>Users</span>
                </a>
            </li>
        </ul>
        <!-- /.sidebar-menu -->

    </section>
    <!-- /.sidebar -->

</aside>
