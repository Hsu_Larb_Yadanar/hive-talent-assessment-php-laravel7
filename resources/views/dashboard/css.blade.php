<link rel="stylesheet" href="{{ asset('css/login_main.css') }}">
<link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
<!-- Date picker-->
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
<!-- Font Awesome -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style media="screen">
  .badge-warning {
    background-color: #fbc994;
    border-color: #fbc994;
    color: #fff;
  }
  .badge-success {
    background-color: #5ac5b6;
    border-color: #5ac5b6;
    color: #fff;
  }
  .badge-danger {
    background-color: #dc3545;
    border-color: #dc3545;
    color: #fff;
  }
  .mb {
    margin-bottom: 20px;
  }
  .select2-container--default .select2-selection--multiple .select2-selection__choice {
    background-color: #58a4da;
    border: 1px solid #95a6d6;
  }
  .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
    color: #fff;
  }
</style>
