<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
    protected $fillable = ['name', 'email', 'phone', 'from_date', 'to_date', 'note', 'barcode_no', 'author_id'];

    public function service()
    {
        return $this->belongsToMany(Services::class,'customer_service','customer_id','service_id');
    }

    public function car()
    {
        return $this->hasMany(Cars::class);
    }

    public function booking()
    {
        return $this->hasMany(Booking::class);
    }
}
