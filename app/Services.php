<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Services extends Model
{
  protected $fillable = ['name','author_id'];

  public function customer()
  {
      return $this->belongsToMany(Customers::class,'customer_service','servcie_id','customer_id');
  }
}
