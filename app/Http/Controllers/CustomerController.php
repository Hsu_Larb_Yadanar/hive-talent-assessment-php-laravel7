<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customers;
use App\Services;
use App\Cars;
use App\Booking;

class CustomerController extends Controller
{
    public function __construct()
    {
          $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Services::all();
        return view('customers.index',compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedCustomer = $this->validateCustomer($request);
        $validatedCustomer['barcode_no'] = $this->generateBarcodeNumber();
        $customer = Customers::create($validatedCustomer + ['author_id' => auth()->id()]);
        $validatedCar = $this->validateCar($request);
        $validatedCar['customer_id'] = $customer->id;
        Cars::create($validatedCar + ['author_id' => auth()->id()]);
        $customer->service()->sync($request->services ,false);
        $bookingData['customer_id'] = $customer->id;
        Booking::create($bookingData + ['author_id' => auth()->id()]);
        // send sms after booking
        $basic  = new \Nexmo\Client\Credentials\Basic('9c070f19', 'DeQ5J9eELTOgLvMo');
        $client = new \Nexmo\Client($basic);
        $message = $client->message()->send([
          'to' => $customer->phone,
          'from' => 'Auto Verge',
          'text' => 'Enjoy. Your booking id is ' . $customer->barcode_no
        ]);
        return redirect()->route('welcome')->with('success', 'Date is successfully saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function validateCustomer($request)
    {
        return $request->validate([
            'name' => 'required|string|max:125',
            'email' => 'required|email',
            'phone' => 'required',
            'from_date' => 'required',
            'to_date' => 'required',
            'note' => 'required'
        ]);
    }

    public function validateCar($request)
    {
        return $request->validate([
            'car_name' => 'required',
            'reg_no' => 'required',
            'model' => 'required',
        ]);
    }

    public function generateBarcodeNumber()
    {
        $number = mt_rand(1000000000, 9999999999);
        if ($this->barcodeNumberExists($number)) {
            return generateBarcodeNumber();
        }
        return $number;
    }

    public function barcodeNumberExists($number)
    {
        return Customers::where('barcode_no')->exists();
    }
}
