<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Booking;
use App\Customers;
use App\Services;
use App\Cars;

class BookingController extends Controller
{
    public function __construct()
    {
        $this->middleware('is_admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bookings = Booking::with('customer')->get();
        return view('backend.booking.index',compact('bookings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $services = Services::all();
        return view('backend.booking.create',compact('services'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedCustomer = $this->validateCustomer($request);
        $validatedCustomer['barcode_no'] = $this->generateBarcodeNumber();
        $customer = Customers::create($validatedCustomer + ['author_id' => auth()->id()]);
        $validatedCar = $this->validateCar($request);
        $validatedCar['customer_id'] = $customer->id;
        Cars::create($validatedCar + ['author_id' => auth()->id()]);
        $customer->service()->sync($request->services ,false);
        $bookingData['customer_id'] = $customer->id;
        Booking::create($bookingData + ['author_id' => auth()->id()]);
        // send sms after booking
        $basic  = new \Nexmo\Client\Credentials\Basic('9c070f19', 'DeQ5J9eELTOgLvMo');
        $client = new \Nexmo\Client($basic);
        $message = $client->message()->send([
          'to' => $customer->phone,
          'from' => 'Auto Verge',
          'text' => 'Enjoy. Your booking id is ' . $customer->barcode_no
        ]);
        return redirect()->route('booking.index')->with('success', 'Date is successfully saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Booking $booking)
    {
        $booking = Booking::with('customer')->findOrFail($booking->id);
        $services = Services::all();
        return view('backend.booking.edit', compact('booking','services'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatebooking = $this->validateBooking($request);
        $validatecustomer = $this->validateCustomer($request);
        $phone = $validatecustomer['phone'];
        Booking::whereId($id)->update($validatebooking + ['author_id' => auth()->id()]);
        $customer = Customers::find($id);
        if (isset($request->services)) {
            $customer->service()->sync($request->services);
        }else{
            $customer->service()->sync(array());
        }
        $customer->update($validatecustomer + ['author_id' => auth()->id()]);
        // send sms after receive price
        $basic  = new \Nexmo\Client\Credentials\Basic('9c070f19', 'DeQ5J9eELTOgLvMo');
        $client = new \Nexmo\Client($basic);
        $message = $client->message()->send([
          'to' => $phone,
          'from' => 'Auto Verge',
          'text' => 'Thanks for using our services.'
        ]);
        return redirect()->route('booking.index')->with('success', 'Data is successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $booking = Booking::findOrFail($id);
      $booking->delete();
      return redirect()->route('booking.index')->with('success', 'Data is successfully deleted');
    }

    public function validateBooking($request)
    {
        return $request->validate([
            'status' => 'required|integer'
        ]);
    }

    public function validateCustomer($request)
    {
        return $request->validate([
            'name' => 'required|string|max:125',
            'email' => 'required|email',
            'phone' => 'required',
            'from_date' => 'required',
            'to_date' => 'required',
            'note' => 'required'
        ]);
    }

    public function validateCar($request)
    {
        return $request->validate([
            'car_name' => 'required',
            'reg_no' => 'required',
            'model' => 'required',
        ]);
    }

    public function generateBarcodeNumber()
    {
        $number = mt_rand(1000000000, 9999999999);
        if ($this->barcodeNumberExists($number)) {
            return generateBarcodeNumber();
        }
        return $number;
    }

    public function barcodeNumberExists($number)
    {
        return Customers::where('barcode_no')->exists();
    }
}
