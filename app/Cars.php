<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cars extends Model
{
    protected $fillable = ['car_name', 'reg_no', 'model', 'customer_id', 'author_id'];

    public function customer()
    {
      return $this->belongsTo(Customers::class);
    }
}
