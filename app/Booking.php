<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $fillable = ['customer_id', 'status', 'author_id'];

    public function customer()
    {
      return $this->belongsTo(Customers::class);
    }
}
